package dev.cornflower;


import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Getter
public class Network {
    private static final double ETA = 1; // Норма обучения

    private final int windowWidth; // ширина окна
    private final int erasCount; // количество эпох

    private final List<Double> learningY; // Известный вектор Y
    private List<Double> weights; // Вектор весов
    private Double standardError = null;

    public Network(int windowWidth, int erasCount, List<Double> learningY) {
        this.windowWidth = windowWidth;
        this.erasCount = erasCount;
        this.learningY = learningY;
        this.weights = Stream.generate(() -> 0.0)
                .limit(windowWidth).collect(Collectors.toCollection(ArrayList::new));
    }

    /*    Функция запускает процесс обучения
        на выходе получается изменнёный вектор весов
        и также будет посчитано значение среднеквадратичной ошибки,
        которое можно получить геттером
     */
    public List<Double> execute() {
        List<List<Double>> matrix = computeMatrix(windowWidth, learningY);
        log.info(String.format("Элементы векторов-столбцов обучающей выборки: %s", matrix));
        List<Double> computedY;
        for (int i = 0; i < erasCount; i++) {
            log.info("Эпоха: {}", i + 1);
            computedY = learningProcessing(matrix);
            log.info("Вычисленный вектор y: {}", computedY);
        }
        log.info("Значение среднеквадратической ошибки: {}", this.standardError);
        return this.weights;
    }

    //    Обучение в течение одной эпохи
    private List<Double> learningProcessing(List<List<Double>> matrix) {
        List<Double> computedY = new ArrayList<>();
        List<Double> column = new ArrayList<>();
        for (int i = 0; i < matrix.size(); i++) {
            log.info("\tШаг обучения: {}", i);
            column = matrix.get(i);
            log.info("\t\t Обучающий вектор-выборка: {}", column);

            double currentY = Math.round(computeY(weights, column) * 1000.0) / 1000.0;
            computedY.add(currentY);
            log.info("\t\t Вычисленное значение Y: {}", currentY);
            log.info("\t\t Вектор весов: {}", weights);
            double error = learningY.get(windowWidth + i) - currentY;
            this.weights = recomputeWeights(column, weights, error);
        }
        this.standardError = countStandardError(computedY, column);
        return computedY;
    }

    private double countStandardError(List<Double> computedY, List<Double> column) {
        double err = 0.0;
        for (int i = 0; i < windowWidth; i++) {
            err += Math.pow(column.get(i) - computedY.get(i), 2);
        }
        return Math.sqrt(err);
    }

    /*
    Формирую обучающую матрицу:
    Первый столбец (строка в многомерном массиве matrix) соответствует первым windowWidth значениям
    Остальные получены сдвигом
     */
    private List<List<Double>> computeMatrix(int windowWidth,
                                             List<Double> learningY) {
        List<List<Double>> matrix = new ArrayList<>();
        for (int i = 0; i < learningY.size() - windowWidth; i++) {
            matrix.add(learningY.subList(i, i + windowWidth));
        }
        return matrix;
    }

    //    Рассчет Y на основании вектора весов
    private double computeY(List<Double> weights, List<Double> column) {
        double y = 0;
        for (int i = 0; i < column.size(); i++) {
            y += column.get(i) * weights.get(i);
        }
        return y;
    }

    //    Перерасчет вектора веса на основании ошибки
    private List<Double> recomputeWeights(List<Double> column, List<Double> weights, double error) {
        List<Double> recomputed = new ArrayList<>();

        for (int i = 0; i < windowWidth; i++) {
            recomputed.add(weights.get(i) + ETA * error * column.get(i));
        }
        return recomputed;
    }
}
