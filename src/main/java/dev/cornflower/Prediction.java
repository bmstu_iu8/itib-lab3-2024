package dev.cornflower;

import com.github.sh0nk.matplotlib4j.Plot;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.DoubleStream;

@Slf4j
@Getter
public class Prediction {
    private final int windowWidth;
    private final int erasCount;
    private final List<Double> weights; // Весовой вектор
    private final List<Double> initialX; // Начальный вектор X для вычисления значений обучающего вектора Y
    private final List<Double> initialY; // Обучающий вектор Y, значения которого вычислены из функции варианта
    private final List<Double> predictedX; // Прогнозный вектор X (от [b, 2b - a])
    private final List<Double> predictedY; // Прогнозный вектор Y  на основе весов


    public Prediction() {
        Scanner in = new Scanner(System.in);

        System.out.println("Введите, пожалуйста, ширину окна: ");
        this.windowWidth = in.nextInt();
        log.info("Ширина окна: {}", windowWidth);

        System.out.println("Введите, пожалуйста, количество эпох");
        this.erasCount = in.nextInt();
        log.info("Количество эпох: {}", erasCount);

        this.initialX = DoubleStream.iterate(1, i -> i < 1.55, i -> i + 0.01)
                .map(d -> Math.round(d * 100.0) / 100.0)
                .boxed().toList();

        log.info("Интервал X: {}", initialX);
        this.initialY = initialX.stream().
                map(Math::cos)
                .map(d -> Math.round(d * 1000.0) / 1000.0).toList();
        log.info(String.format("Известные значения Y: %s", initialY));
        Network network = new Network(windowWidth, erasCount, initialY);
        this.weights = network.execute();
        in.close();

        this.predictedX = DoubleStream.iterate(1.5, i -> i < 2.01, i -> i + 0.01)
                .map(d -> Math.round(d * 100.0) / 100.0)
                .boxed().toList();
        this.predictedY = getPredictedY(weights, predictedX);
    }

    private List<Double> getPredictedY(List<Double> weights, List<Double> x) {
        List<Double> predictedY = new ArrayList<>(List.copyOf(initialY.subList(predictedX.size() - windowWidth, predictedX.size())));
        for (int i = windowWidth; i < x.size(); i++) {
            predictedY.add(computeY(weights, predictedY.subList(predictedY.size() - windowWidth, predictedY.size())));
        }
        return predictedY;
    }

    private double computeY(List<Double> weights, List<Double> column) {
        double y = 0;
        for (int i = 0; i < column.size(); i++) {
            y += column.get(i) * weights.get(i);
        }
        return y;
    }

    @SneakyThrows
    public void drawPlot() {
        Plot plt = Plot.create();
        File picFile = File.createTempFile(
                String.format("resources/plot%dWindowWidth%dEras", windowWidth, erasCount),
                ".png"
        );
        plt.plot().add(initialX, initialY, "o")
                .color("blue")
                .linestyle("-")
                .label("Исходная функция: cosx");
        plt.plot().add(predictedX, predictedY, "o")
                .color("orange")
                .linestyle("--")
                .label("Прогноз функции");
        plt.legend().loc("upper right");
        plt.title(String.format("Лабораторная работа №3. Ширина окна: %d, Количество эпох: %d", windowWidth, erasCount));
        plt.savefig(picFile.getAbsolutePath());
        plt.show();
    }

}
