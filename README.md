# ИТИБ ЛР-3

## Исследование однослойных нейронных сетей на примере моделирования булевых выражений

### Вариант 18

Запуск через Main. В Network.java представлен сам алгоритм обучения нейронной сети (запуск обучения реализуется через
функцию execute()), Prediction.java - логика обработки результатов обучения.

Network.java, Prediction.java находятся в свою очередь в srs/main/java/dev.cornflower

<img src="src/main/resources/5windowWidth500eras.png" alt="first plot">
<img src="src/main/resources/3windowWidth500eras.png" alt="first plot">
<img src="src/main/resources/4windowWidth500eras.png" alt="firs plot">